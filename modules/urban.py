import urllib.parse

import lxml.html

from Ximenez import Helpers, HTTPCache

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("urban", self.urban)

    @Helpers.response
    def urban(self, *term, n=None, nick, privmsg, admin):
        """Look up a term in the Urban Dictionary, or get a random definition. Arguments: [TERM...] [N]"""
        if n is None and term:
            try:
                n = int(term[-1])
            except ValueError:
                n = 0
            else:
                n -= 1
                term = term[:-1]
        elif n is None:
            n = 0
        if term:
            term = " ".join(term)
            ud = HTTPCache.urlopen("http://www.urbandictionary.com/define.php?" + urllib.parse.urlencode({"term":term}))
        else:
            ud = HTTPCache.urlopen("http://www.urbandictionary.com/random.php", False)
        ustr = ud.decode("utf8")
        utree = lxml.html.document_fromstring(ustr)
        words = utree.cssselect("a.word")
        defs = utree.cssselect("div.meaning")
        exs = utree.cssselect("div.example")
        if not words:
            return "{}: Definition not found.".format(term)
        if n > len(words):
            return "Index out of range. {} definitions available.".format(len(words))
        res = "{}: {}".format(self.to_text(words[n]), self.to_text(defs[n]))
        exstr = self.to_text(exs[n])
        if exstr:
            res = "{}\nExamples: {}".format(res, exstr)
        if res.count("\n") > 5:
            res = "\n".join(res.split("\n")[:6]) + " (...)"
        # there are \rs in urbandictionary definitions for reasons that are not
        # entirely clear
        res = res.replace("\r", "\n")
        self.api.say(res, target=nick if privmsg else None)
    
    def to_text(self, e):
        return lxml.html.tostring(e, method="text", encoding=str).strip()
