import functools
import random

from Ximenez import Context, Game, GameHelpers, Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("anagram", self.set_game_state)
        self.game = {}

    @Helpers.response
    def set_game_state(self, enable, difficulty=None, min=5, max=7, *, nick, privmsg, admin):
        """Word game. Descramble anagrams. Arguments: [DIFFICULTY] [MIN LENGTH] [MAX LENGTH]"""
        difficulty = difficulty or "easy"
        if privmsg:
            context = nick
        else:
            context = None
        enable = enable.lower()
        if enable == "on":
            if context in self.game:
                return "Already running."
            try:
                min = int(min)
                max = int(max)
            except ValueError:
                return "min and max must be integers"
            end_signal = functools.partial(self.ended, context)
            try:
                self.game[context] = AnagramGame(AnagramState, Game.Player, self.api, end_signal, difficulty, min, max, context)
            except WordRangeError:
                return "No word matching specified lengths found."
            except GameHelpers.DictionaryError:
                return "Difficulty not recognised."
        elif enable == "off":
            if not context in self.game:
                return "Not running."
            self.game[context].end(True)
        else:
            return "Unknown argument."

    def ended(self, context):
        del self.game[context]


class WordRangeError(GameHelpers.DictionaryError):
    pass


class AnagramGame(GameHelpers.AutoRegistrationGame):
    def begin(self, initial_state, difficulty, minl, maxl, context):
        self.context = context
        # load dictionary
        difficulty = difficulty or "easy"
        self.words = [i for i in GameHelpers.Dictionary(difficulty) if minl <= len(i) <= maxl]
        if not self.words:
            raise WordRangeError("No matching word found.")
        self.target = random.choice(self.words)
        self.words = set(self.words)
        word = list(self.target)
        random.shuffle(word)
        self.anagram = "".join(word)
        self.target_set = sorted(word)
        self.api.say("Anagram enabled. Anagram is '{}'.".format(self.anagram), target=context)
        super().begin(initial_state)

    def end(self, abort=False):
        super().end(abort)
        if abort:
            self.api.say("Anagram aborted. Word was {}.".format(self.target), target=self.context)


class AnagramState(Game.State):
    def setup(self):
        self.listener = Context.MessageContextGuard(self.api, self.check_word, self.game.context)

    def check_word(self, event):
        privmsg = event.type == "privmsg"
        msg = event.arguments[0].split()
        player = event.source.nick
        if len(msg) != 1:
            return
        word = msg[0].lower()
        if not word.isalpha():
            return
        if sorted(word) == self.game.target_set:
            if word in self.game.words:
                self.api.address_say(player, "You win!", privmsg=privmsg)
                self.game.end()
            else:
                self.api.address_say(player, "Not in dictionary.", privmsg=privmsg)
        else:
            self.api.address_say(player, "Not an anagram of {}.".format(self.game.anagram), privmsg=privmsg)

    def finish(self):
        self.listener.unregister()
