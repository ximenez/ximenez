from collections import defaultdict
from datetime import datetime, timedelta, timezone
import os
import pickle
import re

from Ximenez import Helpers

def tz_factory():
    return timezone(timedelta())

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("remind", self.set_reminder, no_kwargs=True)
        self.tzs = defaultdict(tz_factory)
        self.utc = timezone(timedelta())
        self.load_reminders()

    def unload(self):
        self.save_reminders()
        # clear out reminders so they're not duplicated
        for i in range(len(self.api.get_timers(self.tz_say))):
            self.api.unregister_timer(self.tz_say, 0)

    def iter_relevant(self, nick, privmsg):
        timers = self.api.get_timers(self.tz_say)
        for i, t in enumerate(timers):
            if t.function.args[1].lower() == nick and t.function.keywords["privmsg"] == privmsg:
                yield (t, i)

    def save_reminders(self):
        save = {"tz": self.tzs,
                "reminders": []}
        for i in self.api.get_timers(self.tz_say):
            # make sure we strip the actual function called from args
            item = {"t": datetime.fromtimestamp(i.timestamp()), "args": i.function.args[1:], "kwargs": i.function.keywords}
            save["reminders"].append(item)
        with open(os.path.join(self.api.bot.data_dir, "reminders.db"), "wb") as f:
            pickle.dump(save, f)

    def load_reminders(self):
        path = os.path.join(self.api.bot.data_dir, "reminders.db")
        try:
            with open(path, "rb") as f:
                load = pickle.load(f)
        except (FileNotFoundError, EOFError):
            return
        self.tzs = load["tz"]
        for i in load["reminders"]:
            if i["t"] > datetime.now():
                self.api.register_timer(i["t"], self.tz_say, *i["args"], **i["kwargs"])

    @Helpers.response
    def set_reminder(self, cmd, *args, nick, privmsg, admin):
        """Set reminders. Arguments: list | del N | [+][HOURS:]MINUTES[*RECUR_HOURS] [MESSAGE] | tz [TZ]"""
        cmd = cmd.lower()
        nick = nick.lower()
        if cmd == "list":
            lines = ["Your reminders:"]
            for c, (t, i) in enumerate(self.iter_relevant(nick, privmsg), 1):
                if t.function.keywords["recur"]:
                    recur = " (every {} hours)".format(t.function.keywords["recur"])
                else:
                    recur = ""
                lines.append("{}: {}{} - {}".format(c, self.format_t(self.to_local(t, self.tzs[nick])), recur, t.function.args[2].split(": ",1)[1]))
            if len(lines) == 1:
                return "No reminders set."
            return "\n".join(lines)

        if cmd == "del":
            if not args:
                return "You must specify a reminder to delete."
            try:
                n = int(args[0])
            except ValueError:
                return "Must supply an integer."
            for c, (t, i) in enumerate(self.iter_relevant(nick, privmsg), 1):
                if c == n:
                    self.api.unregister_timer(self.tz_say, i)
                    break
            else:
                return "Specified reminder out of range."
            return "Reminder unset."

        if cmd == "tz":
            if not args:
                return "Your timezone is {}".format(self.tzs[nick])
            m = re.match(r"[+-]?(\d{2}):?(\d{2})$", args[0])
            if m:
                self.tzs[nick] = timezone(timedelta(hours=int(m.group(1)), minutes=int(m.group(2))))
            else:
                try:
                    # doesn't match standard pattern, assume it's just hours
                    i = int(args[0])
                except ValueError:
                    return "Supply a timezone in the format +0000."
                self.tzs[nick] = timezone(timedelta(hours=i))
            return "Timezone set to {}.".format(self.tzs[nick])

        match = re.match(r"(\+?)(\d*?):?(\d{1,2})(?:\*(\d+))?$", cmd)
        if not match:
            return "Unknown command. Time format must be [+][HOURS:]MINUTES."
        tz = self.tzs[nick]
        if match.group(2):
            h = int(match.group(2))
        else:
            h = None
        m = int(match.group(3))
        if match.group(4):
            recur = int(match.group(4))
            if not recur:
                # could get a zero here
                recur = None
        else:
            recur = None
        if match.group(1):
            # offset
            if h is None:
                td = timedelta(minutes=m)
            else:
                td = timedelta(hours=h, minutes=m)
            t = datetime.utcnow() + td
            t_tz = self.to_local(t, tz)
        else:
            # absolute
            if h is None:
                return "You must supply an hour for absolute reminders."
            if h >= 24:
                return "Hour must be <24 for absolute reminders."
            t_current = datetime.now(tz) # user's current local time
            t_tz = t_current.replace(hour=h, minute=m, second=0, microsecond=0)
            if t_tz < t_current:
                t_tz += timedelta(days=1)
            t = t_tz.astimezone(self.utc).replace(tzinfo=None) # convert to naive
            td = t - datetime.utcnow()
        self.api.register_timer(t, self.tz_say, nick, "Reminder: " + " ".join(args), privmsg=privmsg, recur=recur)
        self.save_reminders()
        return "Reminder set for {} (in {}).".format(self.format_t(t_tz), self.format_td(td))

    def to_local(self, t, tz):
        return tz.fromutc(t.replace(tzinfo=tz))

    def format_td(self, td):
        h, s = divmod(td.seconds, 3600)
        h += td.days * 24
        m, s = divmod(s, 60)
        return "{:02}:{:02}:{:02}".format(h,m,s)

    def format_t(self, t):
        return t.strftime("%a %H:%M:%S %z")

    def tz_say(self, target, msg, privmsg=False, recur=None):
        self.save_reminders()
        self.api.address_say(target, msg, privmsg=privmsg)
        if recur:
            self.api.register_timer(datetime.utcnow() + timedelta(hours=recur), self.tz_say, target, msg, privmsg=privmsg, recur=recur)
