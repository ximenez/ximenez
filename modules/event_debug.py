from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("eventdbg", self.set_active)

    def event(self, event):
        print("Event:", event.type, event.source.nick, event.target, event.arguments)

    @Helpers.response
    @Helpers.admin
    def set_active(self, enable, *, nick, privmsg, admin):
        """Enable or disable event debugging. Args: on|off"""
        enable = enable.lower()
        if enable == "on":
            self.api.register_listener("__all__", self.event)
            return "Debugging enabled."
        elif enable == "off":
            self.api.unregister_listener("__all__", self.event)
            return "Debugging disabled."
        else:
            return "Unknown state."
