from imp import reload
from importlib import import_module
from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("module", self.module)
        self.api.register_command("die", self.die)
        self.api.register_command("do", self.do, no_kwargs=True)
        self.api.register_command("reimport", self.reimport)
        self.api.register_command("instance", self.instance)

    @Helpers.response
    def module(self, action, mod=None, *, instance="default", nick, privmsg, admin):
        """List/load/unload modules. Arguments: list or load|unload MODULE."""
        if instance != self.api.instance_name:
            return
        action = action.lower()
        if action == "list":
            return "Currently loaded modules: {}".format(", ".join(self.api.bot.modules))
        elif action in ("load", "unload"):
            return self.module_load(action, mod, nick=nick, privmsg=privmsg, admin=admin)
        return "Unknown command."

    @Helpers.admin
    def module_load(self, action, mod, *, nick, privmsg, admin):
        if mod is None:
            return "You must specify a module."
        if action == "load":
            self.api.bot.load_module(mod)
            return "Module loaded."
        self.api.bot.unload_module(mod)
        return "Module unloaded."

    @Helpers.response
    @Helpers.admin
    def die(self, *, instance="default", nick, privmsg, admin):
        """Shut down the bot. Arguments: none."""
        if instance != self.api.instance_name:
            return
        self.api.bot.die()

    @Helpers.response
    @Helpers.admin
    def do(self, *cmd, nick, privmsg, admin):
        """Execute arbitrary commands."""
        d = {"self":self}
        exec(" ".join(cmd), globals(), d)
        if "response" in d:
            return d["response"]
        else:
            return "Command executed successfully."

    @Helpers.response
    @Helpers.admin
    def reimport(self, pkg, *, instance="default", nick, privmsg, admin):
        """Re-import an arbitrary package or module. Useful for reloading helpers. Arguments: package"""
        if instance != self.api.instance_name:
            return
        p = import_module(pkg)
        reload(p)
        return "Reimported."

    @Helpers.response
    def instance(self, *, nick, privmsg, admin):
        """Report currently running instances."""
        return self.api.instance_name
