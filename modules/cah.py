from collections import defaultdict, Counter
import itertools
import random

from Ximenez import Game, GameHelpers, Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("cah", self.set_game_state)
        self.game = None

    @Helpers.response
    def set_game_state(self, enable, bot="on", allvote="on", *, nick, privmsg, admin):
        """Cards Against Humanity. Like Apples to Apples, only horrible. Optionally, have the bot submit a random answer each round, and allow non-players to vote. Arguments: on|off [BOT] [ALLVOTE]"""
        enable = enable.lower()
        if enable == "on":
            if self.game:
                return "Already running."
            self.game = CAHGame(CAHChooseState, CAHPlayer, self.api, self.ended, bot.lower() == "on", allvote.lower() == "on")
        elif enable == "off":
            if not self.game:
                return "Not running."
            self.game.end(True)
        else:
            return "Unknown argument."

    def ended(self):
        self.game = None


class CAHGame(GameHelpers.ExplicitRegistrationGame):
    def begin(self, initial_state, bot=False, allvote=False):
        self.api.say("Cards Against Humanity enabled.")
        self.instructions_shown = False
        self.ending = False
        self.final_round = False
        self.bot_player = bot
        self.all_vote = allvote
        self.score = 0
        self.name = self.api.name
        self.api.register_command("end", self.player_end, raw=True)
        self.api.register_command("scores", self.player_scores, raw=True)
        self.questions = [QuestionCard(i) for i in open("cah-questions.txt", encoding="utf8") if not i.startswith("#")]
        self.answers = [AnswerCard(i) for i in open("cah-answers.txt", encoding="utf8") if not i.startswith("#")]
        self.non_player_voting_history = defaultdict(Counter)
        super().begin(initial_state)

    def items_iter(self, prop, iters):
        for i in sorted(itertools.chain(*iters), key=lambda x: getattr(x, prop), reverse=True):
            yield "{}: {}".format(i.name, getattr(i, prop))

    @Helpers.response
    def player_scores(self, *, nick, privmsg, admin):
        self.list_scores(nick if privmsg else None)

    def list_scores(self, player=None):
        scores = ", ".join(self.items_iter("score", (self.players.values(), self.past_players.values(), [self])))
        self.api.say("Scores: " + scores, target=player, length_override=True)
        rounds = ", ".join(self.items_iter("rounds_played", (self.players.values(), self.past_players.values())))
        self.api.say("Rounds played: " + rounds, target=player)
        hist = []
        for p in itertools.chain(self.players.values(), self.past_players.values()):
            if not p.voting_history:
                continue
            hist.append("{} voted for: {}".format(
                p.name,
                ", ".join("{} {}".format(n.name, v) for n, v in sorted(
                    p.voting_history.items(), key=lambda x: x[1], reverse=True))))
        for voter, c in self.non_player_voting_history.items():
            hist.append("{} voted for: {}".format(
                voter,
                ", ".join("{} {}".format(voted_for.name, v) for voted_for, v in sorted(
                    c.items(), key=lambda x: x[1], reverse=True))))
        self.api.say("Vote history:\n" + "\n".join(hist), target=player, length_override=True)
        redraws = ", ".join(self.items_iter("redraw_count", (self.players.values(), self.past_players.values())))
        self.api.say("Redraw count: " + redraws, target=player)

    def end(self, abort=False):
        super().end(abort)
        self.api.unregister_command("end", raw=True)
        self.api.unregister_command("scores", raw=True)
        if abort:
            # make sure timers don't go off
            self.current_state.finished = True
            self.api.say("CAH aborted.")
        self.list_scores()

    @GameHelpers.players_only
    @Helpers.response
    def player_end(self, *, nick, privmsg, admin):
        if self.ending:
            return "Already ending."
        self.ending = True
        return "Game will end after next turn."


class CAHPlayer(GameHelpers.ExplicitRegistrationPlayer):
    def __init__(self, name, game):
        # score starts from 1 because it's decremented when cards are issued
        # to penalise redrawing
        self.score = 1
        self.cards = []
        self.chosen_cards = []
        self.vote = None
        self.voting_history = game.non_player_voting_history.pop(name, None) or Counter()
        self.redraw_count = 0
        self.rounds_played = 0
        super().__init__(name, game)

    def join(self):
        # get cards if this is a choosing phase
        if isinstance(self.game.current_state, CAHChooseState):
            self.game.current_state.deal(self)
        elif isinstance(self.game.current_state, CAHVoteState):
            self.vote = None
        self.score -= 1

    def leave(self):
        while self.cards:
            c = self.cards.pop()
            c.owner = None
            self.game.answers.append(c)
        self.game.current_state.check_done()


class CAHChooseState(GameHelpers.TimedState):
    length = 120
    def setup(self):
        super().setup()
        self.api.register_command("choose", self.choose, raw=True)
        self.api.register_command("redraw", self.redraw, raw=True)
        if self.game.ending or not self.game.questions:
            # the final round is traditionally this card
            self.game.question = QuestionCard("3Make a haiku.")
            self.game.final_round = True
        else:
            n = random.randrange(len(self.game.questions))
            self.game.question = self.game.questions.pop(n)
        self.hand_size = 10
        if self.game.question.n_answers == 2:
            count = "two"
        elif self.game.question.n_answers == 3:
            count = "three"
            self.hand_size = 13
        else:
            count = "one"
        self.api.say("New question. Say 'choose{}' to choose {} card{} to play. {}Question is:\n{}".format(
            " N" * self.game.question.n_answers,
            count,
            "s" if self.game.question.n_answers > 1 else "",
            "You may say 'redraw' to draw new cards. " if not self.game.instructions_shown else "",
            self.game.question))
        for p in self.game.players.values():
            self.deal(p)

    @Helpers.response
    @GameHelpers.players_only
    def choose(self, *n, nick, privmsg, admin):
        cards = []
        player = self.game.players[nick]
        if len(player.chosen_cards) >= self.game.question.n_answers:
            # start over
            player.chosen_cards = []
        for i in n:
            try:
                i = int(i)
            except ValueError:
                return "You must pick numbers."
            if not 0 < i <= len(player.cards):
                return "You must pick cards between 1 and {}".format(len(player.cards))
            if i-1 in cards or i-1 in player.chosen_cards:
                return "You can't pick the same card more than once."
            cards.append(i-1)
        if len(cards) + len(player.chosen_cards) > self.game.question.n_answers:
            return "You have picked too many answers."
        player.chosen_cards.extend(cards)
        self.check_done()

    @GameHelpers.players_only
    def redraw(self, *, nick, privmsg, admin):
        player = self.game.players[nick]
        player.score -= 1
        while player.cards:
            c = player.cards.pop()
            c.owner = None
            self.game.answers.append(c)
        self.deal(player)
        player.redraw_count += 1

    def deal(self, player):
        player.chosen_cards = []
        while len(player.cards) < self.hand_size:
            if not self.game.answers:
                # out of cards
                if not self.game.final_round:
                    self.game.final_round = True
                    self.api.say("Out of answer cards - game ends this round.")
                return
            n = random.randrange(len(self.game.answers))
            c = self.game.answers.pop(n)
            c.owner = player
            player.cards.append(c)
        self.api.address_say(player.name, "Cards: " + " | ".join("{}: {}".format(i, c) for i, c in enumerate(player.cards, 1)), privmsg=True)

    def check_done(self):
        for p in self.game.players.values():
            if len(p.chosen_cards) != self.game.question.n_answers:
                return
        self.game.go_to_state(CAHVoteState)

    def time_up(self):
        self.game.go_to_state(CAHVoteState)

    def finish(self):
        super().finish()
        self.api.unregister_command("choose", raw=True)
        self.api.unregister_command("redraw", raw=True)


class CAHVoteState(GameHelpers.TimedState):
    length = 120
    def setup(self):
        super().setup()
        self.api.register_command("vote", self.vote, raw=True)
        self.non_player_votes = {}
        self.answers = []
        self.need_votes = []
        for p in self.game.players.values():
            p.vote = None
            if len(p.chosen_cards) != self.game.question.n_answers:
                continue
            p.rounds_played += 1
            self.need_votes.append(p)
            ans = []
            for i in p.chosen_cards:
                ans.append(p.cards[i])
            for i in ans:
                p.cards.remove(i)
            self.answers.append(ans)
        if self.game.bot_player and len(self.game.answers) >= self.game.question.n_answers:
            bot_answer = []
            for i in range(self.game.question.n_answers):
                n = random.randrange(len(self.game.answers))
                c = self.game.answers.pop(n)
                c.owner = self.game
                bot_answer.append(c)
            self.answers.append(bot_answer)
        if not self.answers:
            self.api.say("No answers submitted, going to next question.")
            self.game.go_to_state(CAHChooseState)
        random.shuffle(self.answers)
        self.api.say("The question was:\n{}".format(self.game.question))
        self.api.say("Answers:\n{}".format("\n".join("{}: {}".format(i, " | ".join(str(j) for j in c)) for i, c in enumerate(self.answers, 1))), length_override=True)
        if not self.game.instructions_shown:
            self.api.say("Say 'vote N' to vote. This must be done publicly. You cannot vote for yourself.")
            self.game.instructions_shown = True

    @Helpers.response
    def vote(self, n, *commentary, nick, privmsg, admin):
        if privmsg:
            return "You must do that publicly."
        try:
            n = int(n)
        except ValueError:
            return "You must submit an integer."
        if not 0 < n <= len(self.answers):
            return "You must submit a number between 1 and {}.".format(len(self.answers))
        if nick not in self.game.players:
            if not self.game.all_vote:
                return "You must be in the game to do that."
            else:
                self.non_player_votes[nick] = n-1
                return
        player = self.game.players[nick]
        if self.answers[n-1][0].owner is player:
            return "You can't vote for yourself."
        player.vote = n-1
        self.check_done()

    def check_done(self):
        for p in self.game.players.values():
            if p.vote is None:
                return
        self.award_points()

    def time_up(self):
        self.award_points()

    def award_points(self):
        votes = Counter()
        answers = {}
        for a in self.answers:
            answers[a[0].owner] = a
        for p in self.game.players.values():
            if p.vote is not None:
                voted_for = self.answers[p.vote][0].owner
                votes[voted_for] += 1
                p.voting_history[voted_for] += 1
            elif p in self.need_votes:
                p.score -= 1
                self.api.say("{} loses a point for submitting and not voting.".format(p.name))
        for voter, v in self.non_player_votes.items():
            voted_for = self.answers[v][0].owner
            votes[voted_for] += 1
            self.game.non_player_voting_history[voter][voted_for] += 1
        res = ["Results:"]
        for owner, answer in sorted(answers.items(), key=lambda x: votes.get(x[0], 0), reverse=True):
            res.append("{} got {} for {}".format(owner.name, votes.get(owner, 0), " | ".join(str(i) for i in answer)))
        self.api.say("\n".join(res), length_override=True)
        for p, n in votes.items():
            p.score += n
        if not self.game.final_round:
            self.game.go_to_state(CAHChooseState)
        else:
            self.game.end()

    def finish(self):
        super().finish()
        self.api.unregister_command("vote", raw=True)


class QuestionCard(object):
    def __init__(self, q):
        q = q.strip().replace(r"\n", "\n")
        if q.startswith(("2", "3")):
            self.n_answers = int(q[0])
            q = q[1:]
        else:
            self.n_answers = 1
        self.q = q

    def __str__(self):
        return self.q


class AnswerCard(object):
    def __init__(self, a):
        self.a = a.strip()
        self.owner = None

    def __str__(self):
        return self.a
