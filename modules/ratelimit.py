import fnmatch
import json
import os
import time

from irc.client import NickMask

from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("ratelimit", self.ratelimit_control)
        self.api.register_filter(self.do_filter)
        path = os.path.join(self.api.bot.data_dir, "ratelimit.json")
        if os.path.exists(path):
            with open(path) as f:
                load_masks = json.load(f)
            self.masks = {k: (NickMask(m), r) for k, (m, r) in load_masks.items()}
        else:
            self.masks = {}
        self.timedict = {}

    def unload(self):
        with open(os.path.join(self.api.bot.data_dir, "ratelimit.json"), "w") as f:
            json.dump(self.masks, f)

    @Helpers.response
    @Helpers.admin
    def ratelimit_control(self, action, mask=None, rate=None, *, nick, privmsg, admin):
        """Rate limit a nick mask (rate in seconds between accepted commands). Arguments: set MASK RATE; del MASK; list"""
        action = action.lower()
        if action == "list":
            if not self.masks:
                return "No current rate limit list."
            return "Rate limit list: " + " ; ".join("{} {}".format(k, v[1]) for k, v in self.masks.items())
        elif action in ("set", "remove", "del") and not mask:
            return "You must supply a mask."
        elif action == "set":
            if not rate:
                return "You must supply a rate."
            try:
                rate = float(rate)
            except ValueError:
                return "Rate must be a number."
            if rate <= 0:
                return "Rate must be positive."
            m = NickMask(mask.upper())
            try:
                m.nick
                m.user
                m.host
            except IndexError:
                return "Invalid mask."
            self.masks[mask] = (m, rate)
            return "Added."
        elif action in ("remove", "del"):
            if mask not in self.masks:
                return "Not rate limited."
            del self.masks[mask]
            return "Removed."
        else:
            return "Unknown command."

    def do_filter(self, event):
        if event.type not in ("pubmsg", "privmsg"):
            return False
        if not isinstance(event.source, NickMask):
            return False
        msg = event.arguments[0].strip()
        if not msg or msg[0] != self.api._bot.command_char:
            return False
        mask = NickMask(event.source.upper())
        for i, rate in self.masks.values():
            if (
                    fnmatch.fnmatch(mask.nick, i.nick)
                    and fnmatch.fnmatch(mask.user, i.user)
                    and fnmatch.fnmatch(mask.host, i.host)
                ):
                speaktime = time.time()
                if i in self.timedict:
                    delta = speaktime - self.timedict[i]
                    if delta < rate:
                        return True
                self.timedict[i] = speaktime
                return False
        return False
