import collections
import os
import pickle

from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_listener("pubmsg", self.active)
        self.api.register_listener("join", self.active)
        self.api.register_listener("action", self.active)
        self.api.register_command("pounce", self.set_pounce, no_kwargs=True)
        self.pounces = collections.defaultdict(dict)
        self.capitalisation = {}
        self.load_pounces()

    def unload(self):
        self.save_pounces()

    def active(self, event):
        nick = event.source.nick.lower()
        if nick in self.pounces:
            for person, msg in self.pounces[nick].items():
                self.api.address_say(event.source.nick, "{} says: {}".format(self.capitalisation[person], msg))
            del self.pounces[nick]

    @Helpers.response
    def set_pounce(self, target, *msg, nick, privmsg, admin):
        """Leave a message for a user next time they're active. Arguments: the user and a message (or no message to unset pounce)."""
        target = self.api.nickname_trim(target)
        if not target:
            return "Not a valid target nickname."
        if nick.lower() in self.pounces[target.lower()]:
            prevpounce = self.pounces[target.lower()][nick.lower()]
        else:
            prevpounce = None
        if not msg:
            if prevpounce:
                del self.pounces[target.lower()][nick.lower()]
                return "Pounce for {} unset. Was: {}".format(target, prevpounce)
            return "You do not have a pounce for {} set.".format(target)
        self.pounces[target.lower()][nick.lower()] = " ".join(msg)
        self.capitalisation[nick.lower()] = nick
        if prevpounce:
            prev = " Previous pounce was: {}".format(prevpounce)
        else:
            prev = ""
        return "Pounce for {} set.{}".format(target, prev)

    def save_pounces(self):
        path = os.path.join(self.api.bot.data_dir, "pounces.db")
        with open(path, "wb") as f:
            pickle.dump({"capitalisation":self.capitalisation, "pounces":self.pounces}, f)

    def load_pounces(self):
        path = os.path.join(self.api.bot.data_dir, "pounces.db")
        try:
            with open(path, "rb") as f:
                load = pickle.load(f)
        except (FileNotFoundError, EOFError):
            return
        self.capitalisation = load["capitalisation"]
        self.pounces = load["pounces"]
