import random
import re

from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("roll", self.roll)
        self.api.alias_command("dice", "roll")

    @Helpers.response
    def roll(self, spec, *, nick, privmsg, admin):
        """Roll some dice. Arguments: [N]dX"""
        m = re.match(r"(\d{0,2})[Dd](\d{1,4})$", spec)
        if not m:
            return "Must specify [N]dX dice to roll (e.g. 1d6)."
        if m.group(1):
            n = int(m.group(1))
        else:
            n = 1
        if not n:
            return "Rolled no dice: 0."
        d = int(m.group(2))
        if not d:
            return "Rolled no-sided dice: 0."
        res = sum(random.randint(1, d) for i in range(n))
        return "Rolled {}: {}".format(spec, res)
