import fnmatch
import json
import os

from irc.client import NickMask

from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("ignore", self.ignore_control)
        self.api.register_filter(self.do_filter)
        path = os.path.join(self.api.bot.data_dir, "ignore.json")
        if os.path.exists(path):
            with open(path) as f:
                load_masks = json.load(f)
            self.masks = {k: NickMask(v) for k, v in load_masks.items()}
        else:
            self.masks = {}

    def unload(self):
        with open(os.path.join(self.api.bot.data_dir, "ignore.json"), "w") as f:
            json.dump(self.masks, f)

    @Helpers.response
    @Helpers.admin
    def ignore_control(self, action, mask=None, *, nick, privmsg, admin):
        """Ignore/unignore a nick mask. Arguments: add|remove MASK; list"""
        action = action.lower()
        if action == "list":
            if not self.masks:
                return "No current ignore list."
            return "Ignore list: " + " ; ".join(self.masks)
        elif action in ("add", "remove", "del") and not mask:
            return "You must supply a mask."
        elif action == "add":
            if mask in self.masks:
                return "Already ignored."
            m = NickMask(mask.upper())
            try:
                m.nick
                m.user
                m.host
            except IndexError:
                return "Invalid mask."
            self.masks[mask] = m
            return "Added."
        elif action in ("remove", "del"):
            if mask not in self.masks:
                return "Not ignored."
            del self.masks[mask]
            return "Removed."
        else:
            return "Unknown command."

    def do_filter(self, event):
        if not isinstance(event.source, NickMask):
            return False
        mask = NickMask(event.source.upper())
        for i in self.masks.values():
            if (
                    fnmatch.fnmatch(mask.nick, i.nick)
                    and fnmatch.fnmatch(mask.user, i.user)
                    and fnmatch.fnmatch(mask.host, i.host)
                ):
                return True
        return False
