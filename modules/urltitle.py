import re
import urllib.error

import lxml.html

from Ximenez import HTTPCache

class Module(object):
    def __init__(self, api):
        self.api = api
        # TODO load extra patterns from data file?
        sites = [
                r"( ((m\.)?youtube\.com/watch\?[A-Za-z0-9\-_&=]*?v=) | (youtu\.be/) ) [A-Za-z0-9\-_]{11}",
                r"vimeo\.com/\d{7,10}",
                r"(m\.)?bbc\.co(?:\.uk|m)/((news(beat)?) | (nature) | (sport/\d))/[a-z0-9\-]*/?[0-9]{8}",
                r"xkcd\.com/\d{1,5}",
                ]
        pattern = r"https?://(www\.)?(({}))".format(r") | (".join(sites))
        self.regex = re.compile(pattern, re.X)
        self.api.register_listener("pubmsg", self.title)

    def title(self, event):
        msg = event.arguments[0]
        for m in self.regex.finditer(msg):
            try:
                page = HTTPCache.urlopen(m.group())
            except urllib.error.HTTPError:
                continue
            try:
                page_u = page.decode("utf8")
            except UnicodeDecodeError:
                tree = lxml.html.document_fromstring(page)
            else:
                try:
                    tree = lxml.html.document_fromstring(page_u)
                except ValueError:
                    tree = lxml.html.document_fromstring(page)
            title = tree.cssselect("title")
            if title:
                title = lxml.html.tostring(title[0], method="text", encoding=str).strip()
                self.api.address_say(event.source.nick, title)
