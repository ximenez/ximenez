import ctypes
import glob
import os
import random
import re

from Ximenez import Helpers

# To use HAL you must:
# a) put libmegahal.so in the data dir (there's a script that builds this library
# from the MegaHAL source in utils/)
# b) put megahal.aux, megahal.ban, megahal.swp from the MegaHAL source distribution
# into the data dir
# c) put logs named megahal_seed_* into the data dir
# d) (optional) put a list of words/phrases to exclude as megahal_excludes.txt
# into the data dir
# Note: HAL works in the current directory, so make sure nothing else is chdiring
# out of the data dir

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("hal", self.hal_control)
        self.enabled = False
        self.nrchance = 0.1
        self.rchance = 0.01

    def unload(self):
        if self.enabled:
            self.disable_hal()

    @Helpers.response
    def hal_control(self, status, rebuild="no", *, nick, privmsg, admin):
        """Enable/disable HAL. Arguments: on|off"""
        status = status.lower()
        if status == "on":
            if self.enabled:
                return "HAL already on."
            return self.enable_hal(rebuild, admin=admin)
        elif status == "off":
            if not self.enabled:
                return "HAL is not on."
            self.disable_hal()
            return "HAL disabled."
        else:
            return "Unknown command."

    def enable_hal(self, rebuild, admin):
        if os.path.exists("megahal_excludes.txt"):
            excludes_list = []
            with open("megahal_excludes.txt", encoding="utf8") as f:
                excludes_list = list(f)
            self.excludes = re.compile("|".join(i.strip() for i in excludes_list), re.I)
        else:
            self.excludes = None
        if rebuild.lower() == "yes":
            if not admin:
                return "You don't have permission to do that."
            if os.path.exists("megahal.brn"):
                # clean up existing brain to force regeneration
                os.remove("megahal.brn")
            # first, build training file
            seedlines = set()
            linepattern = re.compile(r"[A-Z][a-z]{2} \d{2} \d{2}:\d{2}:\d{2} <(\w+)> (?:\[\d{2}:\d{2}:\d{2}\] )?(.+)")
            print("Reading logs...")
            for seed in glob.iglob("megahal_seed_*"):
                with open(seed, encoding="utf8") as f:
                    for line in f:
                        m = linepattern.match(line)
                        if m:
                            msg = self.validate_msg(m.group(1), m.group(2).strip())
                            if msg:
                                seedlines.add(msg)

            print("Read logs.")
            if len(seedlines) > 300000: # determined by experiment to be the approx upper limit
                seedlines = random.sample(seedlines, 300000)
            print("Writing training file...")
            with open("megahal.trn", "w", encoding="utf8") as f:
                for i in seedlines:
                    f.write(i + "\n")
            print("Wrote training file.")
        print("Loading library.")
        self.hal = ctypes.cdll.LoadLibrary(os.path.abspath("libmegahal.so"))
        self.hal.megahal_setnoprompt.restype = None
        self.hal.megahal_setnowrap.restype = None
        self.hal.megahal_setnobanner.restype = None
        self.hal.megahal_initialize.restype = None
        self.hal.megahal_do_reply.restype = ctypes.c_char_p
        self.hal.megahal_cleanup.restype = None
        self.hal.megahal_setnoprompt()
        self.hal.megahal_setnowrap()
        self.hal.megahal_setnobanner()
        self.hal.megahal_initialize()
        self.enabled = True
        print("Loaded.")
        self.api.register_listener("privmsg", self.hal_response)
        self.api.register_listener("pubmsg", self.hal_response)
        return "HAL enabled."

    def validate_msg(self, speaker, msg):
        if speaker.lower().startswith(self.api.name.lower()):
            return False
        if not msg:
            return False
        words = msg.split(None, 1)
        if not words:
            return False
        if words[0].lower().startswith(self.api.name.lower()):
            # strip messages directed to bot
            return False
        if words[0].startswith("!"):
            # strip commands
            return False
        if words[0].endswith(":"):
            # strip direction from messages directed to other people
            if len(msg) < 1:
                msg = words[1]
            else:
                return False
        if words[0].startswith("*"):
            return False
        if self.excludes and self.excludes.search(msg):
            return False
        return msg

    def disable_hal(self):
        self.hal.megahal_cleanup()
        del self.hal
        self.enabled = False
        self.api.unregister_listener("privmsg", self.hal_response)
        self.api.unregister_listener("pubmsg", self.hal_response)

    def hal_response(self, event):
        msg = event.arguments[0]
        if not msg:
            return
        if not self.hal_do_response(event, msg):
            msg = self.validate_msg(event.source.nick, msg)
            if msg:
                self.hal.megahal_learn_no_reply(msg.encode("utf8"))

    def hal_do_response(self, event, msg):
        s = msg.split(None, 1)
        if not s:
            return False
        if event.source.startswith("*"):
            # catch messages from ZNC modules
            return False
        if event.type == "privmsg":
            pass
        elif s[0].lower().strip(":,") == self.api.name.lower():
            if len(s) > 1:
                msg = s[1]
            else:
                return False
        elif self.api.name.lower() in msg.lower() and random.random() < self.nrchance:
            pass
        elif random.random() < self.rchance:
            pass
        else:
            return False
        if msg.startswith(self.api.bot.command_char):
            # don't reply to commands in /msg
            return False
        reply = self.hal.megahal_do_reply(msg.encode("utf8")).decode("utf8")
        # a couple of substitutions that clean up case in smileys
        # should probably be scope to configure more
        reply = re.sub(r"[Xx]d+", lambda match: "x" + "D" * match.group().count("d"), reply)
        reply = re.sub(r"[Tt]_+t", lambda match: "T" + "_"*match.group().count("_") + "T", reply)
        self.api.address_say(event.source.nick, reply, privmsg=event.type == "privmsg")
        return True
