from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("ping", self.ping)

    @Helpers.response
    def ping(self, *, nick, privmsg, admin):
        """Check if the bot is responding. Arguments: none."""
        return "Pong."

    def unload(self):
        self.api.unregister_command("ping")
