import datetime
import os

from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("logging", self.set_active)
        self.enabled = False
        self.f = None
        self._real_say = None
        path = os.path.join(self.api.bot.data_dir, "eventlog-enabled")
        if os.path.exists(path):
            os.remove(path)
            self.enable()

    def unload(self):
        if self.enabled:
            self.disable()
            open(os.path.join(self.api.bot.data_dir, "eventlog-enabled"), "w")

    def event(self, event):
        # we register for __all__ events in order to avoid missing commands,
        # but we're only interested in logging actual messages
        if event.type not in ("pubmsg", "privmsg"):
            return
        self.f.write("{} {} {} -> {} {}\n".format(datetime.datetime.today(), event.type, event.source.nick, event.target, event.arguments))
        self.f.flush()

    def say_wrapper(self, *args, **kwargs):
        self.f.write("{} api.say {} {}\n".format(datetime.datetime.today(), args, kwargs))
        self.f.flush()
        return self._real_say(*args, **kwargs)

    def logrotate(self):
        path = os.path.join(self.api.bot.data_dir, "eventlog-today.log")
        if self.f:
            self.f.close()
            os.rename(path, path.replace("today", "yesterday"))
        self.f = open(path, "a", encoding="utf8")
        self.api.register_timer(datetime.timedelta(days=1), self.logrotate)

    @Helpers.response
    @Helpers.admin
    def set_active(self, enable, *, nick, privmsg, admin):
        """Enable or disable logging. Args: on|off"""
        enable = enable.lower()
        if enable == "on" and not self.enabled:
            self.enable()
            return "Debugging enabled."
        elif enable == "off" and self.enabled:
            self.disable()
            return "Debugging disabled."
        else:
            return "Unknown state or invalid state transition."

    def enable(self):
        self.logrotate()
        self._real_say = self.api.say
        self.api.say = self.say_wrapper
        self.api.register_listener("__all__", self.event)
        self.enabled = True

    def disable(self):
        self.enabled = False
        self.f.close()
        self.f = None
        self.api.say = self._real_say
        self.api.unregister_timer(self.logrotate, 0)
        self.api.unregister_listener("__all__", self.event)
