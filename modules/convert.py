import re

import pint

from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("convert", self.convert)
        self.api.alias_command("unit", "convert")
        self.api.alias_command("units", "convert")
        self.ureg = pint.UnitRegistry()

    @Helpers.response
    def convert(self, *expression, sf=None, nick, privmsg, admin):
        """Convert units. Syntax: NUMBER UNIT in UNIT [sf=SIGFIGS]"""
        expr = " ".join(expression)
        conversion = re.match(r"([+\-0-9.ieE]+) ?([A-Za-z*/^\-0-9 _]+?) in ([A-Za-z*/^\-0-9 _]+)", expr)
        if not conversion:
            return "Unrecognised expression. Syntax: NUMBER UNIT in UNIT"
        try:
            n = float(conversion.group(1))
        except TypeError:
            return "Number format not recognised."
        if sf is not None:
            try:
                sf = int(sf)
            except ValueError:
                return "Number of significant figures must be a positive integer."
            if sf <= 0:
                return "Number of significant figures must be a positive integer."
        else:
            # default to the largest of 3 or one greater than the input precision
            orig = conversion.group(1).lower()
            if "e" in orig:
                orig = orig.split("e")[0]
            sf = max(3, len(orig.lstrip("0.").replace(".", "")) + 1)
        if sf > 10:
            sf = 10
        try:
            u = self.ureg(conversion.group(2)) * n
        except pint.UndefinedUnitError:
            return "Unknown source unit."
        except Exception as e:
            return "Error: {}. Please stop.".format(e)
        try:
            res = u.to(conversion.group(3))
        except pint.UndefinedUnitError:
            return "Unknown destination unit."
        except pint.DimensionalityError:
            return "Incompatible units."
        except Exception as e:
            return "Error: {}. Please stop.".format(e)
        return "{{:.{}g}} {{}}".format(sf).format(res.magnitude, res.units)
