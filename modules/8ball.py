import random

from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("8ball", self.eight_ball)

    responses = [
            "As I see it, yes.",
            "It is certain.",
            "It is decidedly so.",
            "Most likely.",
            "Outlook good.",
            "Signs point to yes.",
            "Without a doubt.",
            "Yes.",
            "Yes - definitely.",
            "You may rely on it.",
            "Reply hazy, try again.",
            "Ask again later.",
            "Better not tell you now.",
            "Cannot predict now.",
            "Concentrate and ask again.",
            "Don't count on it.",
            "My reply is no.",
            "My sources say no.",
            "Outlook not so good.",
            "Very doubtful."
            ]

    @Helpers.response
    def eight_ball(self, *args, nick, privmsg, admin):
        """A magic eight-ball. Answers yes/no questions. Arguments: a question."""
        return random.choice(self.responses)
