import urllib.parse

import lxml.html

from Ximenez import Helpers, HTTPCache

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("dictionary", self.lookup)
        self.api.alias_command("define", "dictionary")

    @Helpers.response
    def lookup(self, *term, part=None, n=None, nick, privmsg, admin):
        """Look up a term on Dictionary.com. Arguments: TERM... [part=PART_OF_SPEECH] [n=N]"""
        term = " ".join(term)
        url = "http://dictionary.reference.com/browse/" + urllib.parse.quote(term)
        ud = HTTPCache.urlopen(url, maxage=86400*7)
        ustr = ud.decode("utf8")
        utree = lxml.html.document_fromstring(ustr)

        res = []
        # get the main definition sections
        defs = utree.cssselect("section.luna-box")
        if not defs:
            return "Definition not found."
        if len(defs) > 1 and n is None:
            res.append("{} definitions found, listing #1 (use n=N for others).".format(len(defs), n))
        if n is None:
            n = 1
        else:
            try:
                n = int(n)
            except ValueError:
                return "Definition number must be an integer."
        if not 0 < n <= len(defs):
            return "Definition must be between 1 and {}".format(len(defs))
        d = defs[n-1]

        defres = []
        # get word
        word = self.to_text(d.cssselect("span.me")[0])

        # get pronunciation
        pron = self.to_text(d.cssselect("span.spellpron")[0])

        # get part of speech sections
        parts = d.cssselect("section.def-pbk")
        realparts = []
        partnames = []
        for i in parts:
            try:
                partnames.append(self.to_text(i.cssselect("span.dbox-pg")[0]).strip(",").lower())
            except IndexError:
                # usage notes and things that aren't really definitions
                pass
            else:
                realparts.append(i)
        parts = realparts
        if part is None:
            if len(parts) > 1:
                res.append("Available parts of speech: {}. (Use part=PART for others.)".format(", ".join(partnames)))
            chosenpart = parts[0]
        elif part.lower() in partnames:
            chosenpart = parts[partnames.index(part.lower())]
        else:
            return "No definitions for that part of speech. Available parts: {}.".format(", ".join(partnames))

        # part of speech and plural
        chosenpartname = self.to_text(chosenpart.cssselect("header.luna-data-header")[0])
        defres.append("{} {}; {}".format(word, pron, chosenpartname))

        # actual definitions
        for i in chosenpart.cssselect("div.def-set"):
            defres.append(self.to_text(i).replace("\n", " "))

        self.api.address_say(nick, " ".join(res), privmsg=privmsg)
        if len(defres) > self.api.maxlines:
            defres[self.api.maxlines-1:] = ["<long definition truncated - see {} >".format(url)]
        return "\n".join(defres)
    
    def to_text(self, e):
        return lxml.html.tostring(e, method="text", encoding=str).strip()
