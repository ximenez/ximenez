import os
import random

from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("quote", self.quote, no_kwargs=True)
        self.load()

    def load(self):
        path = os.path.join(self.api.bot.data_dir, "quotes.txt")
        if not os.path.exists(path):
            self.f = open(path, "w", encoding="utf8")
            self.quotes = []
        else:
            self.f = open(path, "r+", encoding="utf8")
            self.quotes = list(self.f)

    def unload(self):
        self.f.close()

    @Helpers.response
    def quote(self, action=None, *args, nick, privmsg, admin):
        """Quotes database: Arguments: none (random quote) | {n} (specified numbered quote) | count (number of quotes) | search string... (find a quote) | add quote... (add a quote)"""
        target = nick if privmsg else None
        if action is None:
            # return random quote
            if self.quotes:
                self.api.say(random.choice(self.quotes), target=target)
                return
            return "No quotes in database."
        action = action.lower()
        if action == "add":
            return self.add_quote(*args, nick=nick, privmsg=privmsg, admin=admin)
        if action == "search":
            # this isn't particularly optimised..
            matches = []
            query = " ".join(args).lower()
            for i in self.quotes:
                if query in i.lower():
                    matches.append(i)
            if matches:
                self.api.say(random.choice(matches), target=target)
                return
            return "No matching quotes found."
        if action == "count":
            return "There are {} quotes in the database.".format(len(self.quotes))
        if action == "rehash":
            return self.rehash(nick=nick, privmsg=privmsg, admin=admin)
        try:
            i = int(action)
        except ValueError:
            return "Unknown argument."
        try:
            self.api.say(self.quotes[i], target=target)
            return
        except IndexError:
            return "No such ID. Range: 0 - {}.".format(len(self.quotes)-1)

    @Helpers.admin
    def add_quote(self, *quote, nick, privmsg, admin):
        q = " ".join(quote)
        self.quotes.append(q)
        self.f.write(q + "\n")
        self.f.flush()
        return "Quote added."
    
    @Helpers.admin
    def rehash(self, *, nick, privmsg, admin):
        # reload quotes from file in case of external editing
        self.unload()
        self.load()
        return "Rehashed."
