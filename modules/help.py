from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("help", self.do_help)

    @Helpers.response
    def do_help(self, cmd=None, *, nick, privmsg, admin):
        """Provide help on a command or list available commands. Arguments: [command]"""
        # TODO allow annotation of function parameters and generate argument lists?
        if cmd is None:
            # list all commands
            # skip raw commands, which should be transient (during a game only),
            # and aliases
            cmds = []
            for i in self.api._command_registry:
                if i.startswith(self.api.bot.command_char):
                    if not isinstance(self.api._command_registry[i], str):
                        cmds.append(i)
            return "Available commands: {}".format(", ".join(cmds))
        cmd = cmd.lower()
        if cmd not in self.api._command_registry:
            with_cc = self.api.bot.command_char+cmd
            if with_cc in self.api._command_registry:
                cmd = with_cc
            else:
                return "{}: Unknown command.".format(cmd)
        while isinstance(self.api._command_registry[cmd], str):
            cmd = self.api._command_registry[cmd]
        fn = self.api._command_registry[cmd]
        if fn.__doc__ is None:
            return "{}: No help available.".format(cmd)
        return "{}: {}".format(cmd, fn.__doc__)
