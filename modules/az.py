import bisect
import functools
import random

from Ximenez import Context, Game, GameHelpers, Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("az", self.set_game_state)
        self.game = {}

    @Helpers.response
    def set_game_state(self, enable, difficulty=None, *, nick, privmsg, admin):
        """A word guessing game. Work out which word the bot has chosen by shrinking the range of possibilities. Arguments: on|off [DIFFICULTY]"""
        difficulty = difficulty or "easy"
        if privmsg:
            context = nick
        else:
            context = None
        enable = enable.lower()
        if enable == "on":
            if context in self.game:
                return "Already running."
            end_signal = functools.partial(self.ended, context)
            try:
                self.game[context] = AzGame(AzState, AzPlayer, self.api, end_signal, difficulty, context)
            except GameHelpers.DictionaryError:
                return "Difficulty not recognised."
        elif enable == "off":
            if not context in self.game:
                return "Not running."
            self.game[context].end(True)
        else:
            return "Unknown argument."

    def ended(self, context):
        del self.game[context]


class AzGame(GameHelpers.AutoRegistrationGame):
    def begin(self, initial_state, difficulty, context):
        self.context = context
        # load dictionary
        difficulty = difficulty or "easy"
        self.words = GameHelpers.Dictionary(difficulty)
        self.target = random.randrange(len(self.words))
        self.low = 0
        self.high = len(self.words) - 1
        self.api.say("Az enabled.", target=context)
        super().begin(initial_state)

    def end(self, abort=False):
        super().end(abort)
        self.api.say("{}Word was {}. Scores: {}".format(
            "Az aborted. " if abort else "",
            self.words[self.target],
            ", ".join("{}: {}".format(i.name, i.score) for i in set(self.players.values()))),
            target=self.context)


class AzPlayer(Game.Player):
    def __init__(self, name):
        super().__init__(name)
        self.score = 0

    def __add__(self, other):
        new = AzPlayer(self.name)
        new.score = self.score + other.score
        return new


class AzState(Game.State):
    def setup(self):
        self.listener = Context.MessageContextGuard(self.api, self.check_word, self.game.context)
        self.api.say("Range: {} - {} ({})".format(
            self.game.words[self.game.low],
            self.game.words[self.game.high],
            self.game.high - self.game.low - 1),
            target=self.game.context)

    def check_word(self, event):
        privmsg = event.type == "privmsg"
        msg = event.arguments[0].split()
        player = event.source.nick
        if len(msg) != 1:
            return
        word = msg[0].lower()
        if word == self.game.words[self.game.target]:
            self.api.address_say(player, "You win!", privmsg=privmsg)
            self.game.players[player].score += 5
            self.game.end()
            return
        pos = bisect.bisect_left(self.game.words, word, self.game.low, self.game.high + 1)
        if self.game.low < pos <= self.game.high:
            if word in self.game.words[self.game.low:self.game.high+1]:
                self.game.players[player].score += 1
                if pos < self.game.target:
                    self.game.low = pos
                else:
                    self.game.high = pos
                self.game.go_to_state(AzState)
            else:
                self.api.address_say(player, "Not in dictionary: {}".format(word), privmsg=privmsg)

    def finish(self):
        self.listener.unregister()
