from Ximenez import Helpers

class Module(object):
    def __init__(self, api):
        self.api = api
        self.api.register_command("maths", self.maths)
        self.api.alias_command("math", "maths")
        self.legal = set(" +-*/()0123456789.j&|^%")

    @Helpers.response
    def maths(self, *expression, nick, privmsg, admin):
        """Do arithmetic. Arguments: a mathematical expression."""
        expr = " ".join(expression)
        for i in expr:
            if i not in self.legal:
                return "Numbers and mathematical operators only, please."
        # local namespace required for exec to return useful results
        d = {"j":1j}
        try:
            exec("response = " + expr, globals(), d)
        except ZeroDivisionError:
            return "Oh shit, someone divided by zero!"
        except Exception as e:
            return "Error: {}. Please stop.".format(e)
        return str(d["response"])
