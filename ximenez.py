#! /usr/bin/env python3
import argparse

from Ximenez.Ximenez import Ximenez

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--conf", default="ximenez.conf")
args = parser.parse_args()

bot = Ximenez(args.conf)
bot.start()
