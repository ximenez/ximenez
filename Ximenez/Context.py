"""Helpers for managing context-dependent state."""

class MessageContextGuard(object):
    """Context guard for public/private message signals."""
    def __init__(self, api, fn, context):
        self.api = api
        self.fn = fn
        self.context = context
        self.__func__ = fn.__func__
        self.api.register_listener("pubmsg", self)
        self.api.register_listener("privmsg", self)

    def __call__(self, event):
        # determine context from event
        if event.type == "pubmsg":
            if self.context == None:
                self.fn(event)
        else:
            if event.source.nick == self.context:
                self.fn(event)

    def unregister(self):
        self.api.unregister_listener("pubmsg", self)
        self.api.unregister_listener("privmsg", self)


# TODO CommandDispatcher
