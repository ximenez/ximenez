import datetime

from irc import schedule

# HACK! make schedule's datetimes timezone naive again
# TODO remove my hackery here and use the proper timezone aware datetimes

schedule.now = datetime.datetime.now
schedule.from_timestamp = datetime.datetime.fromtimestamp

class XimenezCommand(schedule.DelayedCommand):
    """A DelayedCommand that sticks to UTC."""
    def due(self):
        return self.utcnow() >= self

    @classmethod
    def now(cls):
        return cls.utcnow()
