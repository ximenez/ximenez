"""Decorators and helper functions for writing modules."""
from functools import wraps

def response(fn):
    """Decorator: Mark a function as a response.

    This will automatically address a value returned from the function to the
    user whose message prompted it, in the correct context."""
    @wraps(fn)
    def f(self, *args, **kwargs):
        res = fn(self, *args, **kwargs)
        if res is not None:
            self.api.address_say(kwargs["nick"], res, privmsg=kwargs["privmsg"])
    return f

def admin(fn):
    """Decorator: Mark a command as admin only."""
    @wraps(fn)
    def f(self, *args, **kwargs):
        if kwargs["admin"]:
            return fn(self, *args, **kwargs)
        else:
            self.api.address_say(kwargs["nick"], "You don't have permission to do that.", privmsg=kwargs["privmsg"])
    return f

def targeted(fn):
    """Decorator: Mark a function as being directed at another user."""
    # note - this ignores privmsg and speaks in channel if a target is supplied,
    # to avoid directing private messages at random network-wide users and
    # causing issues
    @wraps(fn)
    def f(self, *args, **kwargs):
        msg = fn(self, *args, **kwargs)
        if args and args[0]:
            self.api.address_say(args[0], msg)
        else:
            self.api.address_say(kwargs["nick"], msg, privmsg=kwargs["privmsg"])
    return f
