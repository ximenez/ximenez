"""Derived bot game classes providing some automated functionality."""

from collections import defaultdict
from datetime import timedelta
from functools import wraps

from Ximenez import Game, Helpers

class game_defaultdict(defaultdict):
    """Subclass of defaultdict which provides the missing key as an argument
    to the factory function."""
    def __missing__(self, key):
        if self.default_factory:
            self[key] = self.default_factory(key)
            return self[key]
        raise KeyError(key)


class NickChangeTrackingGame(Game.Game):
    """A game which automatically tracks nickname changes.

    Player may optionally be subclassed to implement an __add__ method. If this
    exists, it is used to deal with nickname changes (for example in the case
    of connection problems)."""
    def begin(self, initial_state, *args):
        # ugly hack to ensure commands are registered to the right module
        module = self.begin.__func__.__module__
        self.nick_change.__func__.__module__ = module
        self.api.register_listener("nick", self.nick_change)
        super().begin(initial_state)

    def nick_change(self, event):
        orig = event.source.nick
        new = event.target
        if orig in self.players and new in self.players:
            try:
                combined_player = self.players[orig] + self.players[new]
            except TypeError:
                pass
            else:
                self.players[orig] = combined_player
                self.players[new] = combined_player
        elif orig in self.players:
            self.players[new] = self.players[orig]
        elif new in self.players:
            self.players[orig] = self.players[new]

    def end(self, abort=False):
        self.api.unregister_listener("nick", self.nick_change)
        super().end(abort)


class AutoRegistrationGame(NickChangeTrackingGame):
    """A game which does not require explicit registration.

    Anyone can take part, and a new Player will be created for them if one does
    not already exist when they join in.
    People joining and leaving the channel are ignored - the game should not
    require further participation from anybody."""
    def begin(self, initial_state, *args):
        self.players = game_defaultdict(self.player_type)
        super().begin(initial_state)


def players_only(fn):
    """Decorator: Mark a function as callable only by game players.

    If the caller is not a player, an error is returned.."""
    @wraps(fn)
    def f(self, *args, **kwargs):
        if hasattr(self, "players"):
            players = self.players
        else:
            players = self.game.players
        if kwargs["nick"] not in players:
            self.api.address_say(kwargs["nick"], "You must be in the game to do that.", privmsg=kwargs["privmsg"])
            return
        return fn(self, *args, **kwargs)
    return f

class ExplicitRegistrationGame(NickChangeTrackingGame):
    """A game which requires people to explicitly register.

    This can happen at the beginning of the game or optionally while it's in
    progress.
    People leaving the channel are temporarily removed from the game. They are
    not automatically rejoined when they re-enter."""
    def begin(self, initial_state, *args):
        # ugly hack to ensure commands are registered to the right module
        module = self.begin.__func__.__module__
        self.add_player.__func__.__module__ = module
        self.player_leaves.__func__.__module__ = module
        self.person_quits.__func__.__module__ = module
        self.api.register_command("join", self.add_player, raw=True)
        self.api.register_command("leave", self.player_leaves, raw=True)
        self.api.register_listener("part", self.person_quits)
        self.api.register_listener("quit", self.person_quits)
        self.initial_state = initial_state
        self.past_players = {}
        super().begin(RegistrationState, *args)

    def add_player(self, *, nick, privmsg, admin):
        if nick in self.players:
            self.api.address_say(nick, "You are already playing.")
            return
        if nick in self.past_players:
            self.players[nick] = self.past_players[nick]
            del self.past_players[nick]
            self.players[nick].join()
        else:
            self.players[nick] = self.player_type(nick, self)
        self.api.say("{} has joined the game.".format(nick))

    def remove_player(self, player):
        if player not in self.players:
            return
        self.past_players[player] = self.players[player]
        del self.players[player]
        self.past_players[player].leave()
        self.api.say("{} has left the game.".format(player))

    @players_only
    def player_leaves(self, *, nick, privmsg, admin):
        self.remove_player(nick)

    def person_quits(self, event):
        self.remove_player(event.source.nick)

    def end(self, abort=False):
        self.api.unregister_command("join", raw=True)
        self.api.unregister_command("leave", raw=True)
        self.api.unregister_listener("part", self.person_quits)
        self.api.unregister_listener("quit", self.person_quits)
        super().end(abort)


class ExplicitRegistrationPlayer(Game.Player):
    def __init__(self, name, game):
        super().__init__(name)
        self.game = game
        self.join()

    def join(self):
        """Do setup for joining/rejoining the game."""
        pass

    def leave(self):
        """Take actions necessary to leave the game."""
        pass


class RegistrationState(Game.State):
    """State when initial registration for a game is happening."""
    def setup(self):
        module = self.game.begin.__func__.__module__
        self.start.__func__.__module__ = module
        self.api.register_command("start", self.start, raw=True)
        self.api.say("Say 'join' to join. Say 'start' to begin.")

    @Helpers.response
    def start(self, *, nick, privmsg, admin):
        if privmsg:
            return "You must do this publicly."
        self.game.go_to_state(self.game.initial_state)

    def finish(self):
        self.api.unregister_command("start", raw=True)


class GameModule(object):
    """Simple base module for running games.

    Needs to be subclassed and attributes for the game in question set."""
    def __init__(self, api):
        self.api = api
        self.api.register_command(self.game_name, self.set_game_state)
        self.game = None

    @Helpers.response
    def set_game_state(self, enable, *, nick, privmsg, admin):
        enable = enable.lower()
        if enable == "on":
            if self.game:
                return "Already running."
            self.game = self.game_type(self.initial_state, self.player_type, self.api)
        elif enable == "off":
            if not self.game:
                return "Not running."
            self.game.end(True)
        else:
            return "Unknown argument."


class TimedState(Game.State):
    """A state which automatically ends after a set amount of time."""
    def setup(self):
        super().setup()
        self.api.register_timer(timedelta(seconds=self.length-30), self.time_warn)
        self.api.register_timer(timedelta(seconds=self.length), self.time_up_guard)
        self.finished = False

    def time_warn(self):
        if not self.finished:
            self.api.say("30 seconds left.")

    def time_up_guard(self):
        if not self.finished:
            self.api.say("Time up!")
            self.time_up()

    def time_up(self):
        pass

    def finish(self):
        self.finished = True
        super().finish()


class DictionaryError(NameError):
    pass


def Dictionary(name, proper_nouns=False, alphabetical=True):
    """A simple words list. Looks under data/dictionary-{name}.txt."""
    try:
        words = [i.strip() for i in open("dictionary-{}.txt".format(name), encoding="utf8") if i.strip()]
    except OSError:
        raise DictionaryError("dictionary-{}.txt inaccessible".format(name))
    if not proper_nouns:
        # assumes these start with a capital letter in the dictionary
        words = [i for i in words if not i[0].isupper()]
    if alphabetical:
        words_new = []
        for i in words:
            if i.isalpha():
                try:
                    i.encode("ascii")
                except UnicodeEncodeError:
                    pass
                else:
                    words_new.append(i)
        words = words_new
    return words
