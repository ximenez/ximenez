"""Base classes for implementing bot games."""

class Game(object):
    """An object holding the global state for a game."""
    def __init__(self, initial_state, player_type, api, end_signal, *args):
        self.api = api
        self.end_signal = end_signal
        self.player_type = player_type
        # this is a dictionary mapping of nick: Player
        # player management is left up to subclasses as there are a number of
        # different valid approaches
        self.players = {}
        self.begin(initial_state, *args)

    def begin(self, initial_state, *args):
        """Set up the game and move to the first game state."""
        self.current_state = initial_state(self, self.api)

    def go_to_state(self, state, *args, **kwargs):
        """Advances the game to a given state.

        Additional arguments/keyword arguments are passed to the constructor of
        the new state."""
        self.current_state.finish()
        self.current_state = state(self, self.api, *args, **kwargs)

    def end(self, abort=False):
        """End the game. Total up scores, etc.
        
        The abort argument should be set true when a game is forcibly aborted,
        rather than ending naturally."""
        self.current_state.finish()
        self.end_signal()


class Player(object):
    """A player in a game."""
    def __init__(self, name):
        self.name = name


class State(object):
    """A single phase, round, turn, etc of a game."""
    def __init__(self, game, api, *args, **kwargs):
        self.game = game
        self.api = api
        self.setup(*args, **kwargs)

    def setup(self):
        """Register commands, etc for this state."""
        pass

    def finish(self):
        """Unregister commands for this state."""
        pass
