"""Tool for caching HTTP requests.

This allows, for example, the dictionary modules, which generally only return
part of the page due to length limitations, to avoid repeatedly re-requesting
the same page."""

from datetime import datetime, timedelta
import urllib.request

class HTTPCache(object):
    def __init__(self, maxsize=2**20):
        self.data = {}
        self.age = {}
        self.maxsize = maxsize

    def urlopen(self, url, cache=True, maxage=86400):
        """Open and read a URL, using a cache.

        Retrieving from the cache may be disabled, forcing new data to be
        requested. A maximum age of cached data may be supplied, which defaults
        to 1 day."""
        now = datetime.now()
        delta = timedelta(seconds=maxage)
        if cache and url in self.data and now - delta < self.age[url]:
            return self.data[url]
        r = urllib.request.urlopen(url)
        d = r.read()
        if cache:
            self.data[url] = d
            self.age[url] = now
            self.check_size()
        return d
    
    def check_size(self):
        # delete oldest entries until the size is acceptable
        ages = None
        while sum(len(i) for i in self.data.values()) > self.maxsize:
            if ages is None:
                ages = sorted(self.age, key=lambda x: self.age[x], reverse=True)
            oldest = ages[-1]
            del self.data[oldest]
            del self.age[oldest]
            del ages[-1]


cache = HTTPCache()
urlopen = cache.urlopen
