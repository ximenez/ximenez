# Copyright 2013 Sam Lade

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import collections
import configparser
import datetime
import functools
import importlib
import inspect
import os
import re
import shlex
import signal
import ssl
import sys
import time
import traceback

from irc import connection, client, bot as ircbot

try: import setproctitle
except ImportError: pass
else: setproctitle.setproctitle("ximenez")

from .Schedule import XimenezCommand

VERSION = "3.0.0b2"

def encoding_fallback(exception):
    """Handle clients using weird encoding

    Fall back to windows-1252, and if that fails, replace error characters"""
    try:
        m = exception.object[exception.start:exception.end].decode("windows-1252")
    except UnicodeDecodeError:
        return codecs.replace_errors(exception)
    return m, exception.end
codecs.register_error("fallback", encoding_fallback)

class Ximenez(ircbot.SingleServerIRCBot):
    def __init__(self, config_path: "Path to config file"):
        if not os.path.exists(config_path):
            print("Error: Must supply a configuration file.")
            raise SystemExit(1)
        config = configparser.SafeConfigParser()
        config.read(config_path)
        xconf = config["ximenez"]

        # comma separated list of hosts
        self.admins = set(xconf["admins"].split(","))
        server = xconf["server"]
        port = xconf.getint("port", 6667)
        password = xconf.get("password")
        secure = xconf.getboolean("secure")

        nickname = xconf["nickname"]
        realname = xconf.get("realname", nickname)
        self.channel = xconf["channel"] # TODO support multiple channels
        self.command_char = xconf.get("command", "!")

        self.data_dir = os.path.abspath(xconf.get("data_dir", "data"))
        self.mod_dirs = [os.path.abspath(i) for i in xconf.get("mod_dir", "modules,site-modules").split(",")]
        # make sure modules put their files in the right place
        if not os.path.exists(self.data_dir):
            os.makedirs(self.data_dir)
        os.chdir(self.data_dir)

        self.whitelist = xconf.get("whitelist", None)
        self.blacklist = xconf.get("blacklist", None)
        self.instance_name = xconf.get("instance", "default")
        self.verbose_debug = xconf.getint("debug", 2)

        # support secure connections by SSL wrapping socket
        fac = connection.Factory(wrapper=ssl.wrap_socket) if secure else connection.Factory()
        super().__init__([(server, port, password)], nickname, realname, 1, connect_factory=fac)

        self.api = API(self)

        self.reactor.add_global_handler("all_events", self.api_dispatch, -9)

        self.modules = {}
        self.load_modules()

    def start(self):
        """Main bot event loop.

        Automatically cleans up on exit of event loop."""
        # install sigterm handler so clean shutdown runs in that case
        signal.signal(signal.SIGTERM, self.abort)
        try:
            super().start()
        finally:
            print()
            self.unload_modules()

    def abort(self, sign, frame):
        raise SystemExit(0)

    def _connect(self):
        super()._connect()
        # bit of a hack to install the codec error handling on the connection
        # this avoids nasty crashes on non-UTF8 input
        if self.reactor.connections:
            self.reactor.connections[0].buffer.errors = "fallback"

    def api_dispatch(self, c, event):
        """Dispatch events to handlers registered by the API."""
        # Some events whose sources are full NickMasks don't get the type,
        # which is irritating. I could make a full list of event types that
        # should have nickmasks. Or I could just try it on all of them!
        source_mask = client.NickMask(event.source)
        try:
            source_mask.nick
            source_mask.host
            source_mask.user
        except IndexError:
            event.source = str(source_mask)
        else:
            event.source = source_mask

        # check the special all events handler and post if present
        # use this with caution!
        if "__all__" in self.api._listener_registry:
            for i in self.api._listener_registry["__all__"]:
                try:
                    i(event)
                except:
                    self.api.say(self.handle_exception())

        # eat filtered events
        for i in self.api._filter_registry:
            try:
                if i(event):
                    return
            except:
                self.api.say(self.handle_exception())

        # special case messages in order to execute commands
        if event.type in ("pubmsg", "privmsg"):
            cmd = event.arguments[0].split()
            # note - case insensitivity is enforced here
            if len(cmd) > 1:
                cmd, arg = cmd[0].lower(), cmd[1:]
            elif len(cmd) == 1:
                cmd, arg = cmd[0].lower(), []
            else:
                cmd, arg = None, []
            # if event is not a command, we fall through and post it to handlers
            # as usual
            if cmd in self.api._command_registry:
                # check for aliasing
                while isinstance(self.api._command_registry[cmd], str):
                    cmd = self.api._command_registry[cmd]
                kwargs = {}
                if cmd not in self.api._no_kwargs:
                    # use shlex to allow multi-word kwargs etc
                    try:
                        lexarg = shlex.split(" ".join(arg))
                    except ValueError:
                        # this is probably an unmatched quotation mark etc
                        # we just give up on shell lexing, though we still try
                        # and locate kwargs
                        lexarg = arg
                    args = []
                    for i in lexarg:
                        if "=" in i:
                            k, v = i.split("=")
                            kwargs[k] = v
                        else:
                            args.append(i)
                else:
                    args = arg
                # fill in metadata kwargs
                kwargs["nick"] = event.source.nick
                kwargs["privmsg"] = event.type == "privmsg"
                kwargs["admin"] = event.source.host in self.admins
                # verify signature match
                fn = self.api._command_registry[cmd]
                sig = inspect.signature(fn)
                try:
                    sig.bind(*args, **kwargs)
                except TypeError:
                    # call will fail
                    # unfortunately the errors from Signature.bind() and
                    # inspect.getcallargs() aren't as helpful as the errors from
                    # actually calling, so we do that to get the error message
                    try:
                        fn(*args, **kwargs)
                    except TypeError as e:
                        self.api.address_say(event.source.nick, "{} {}".format(cmd, e.args[0].split(None, 1)[1]), privmsg=kwargs["privmsg"])
                    return
                try:
                        fn(*args, **kwargs)
                except SystemExit:
                    raise
                except:
                    # exceptions in modules should not crash the core
                    self.api.say(self.handle_exception(), target=kwargs["nick"] if kwargs["privmsg"] else None)
                return

        # handle general events
        if event.type in self.api._listener_registry:
            # copy to prevent runtime errors if handlers are added/removed
            reg_copy = self.api._listener_registry[event.type].copy()
            for i in reg_copy:
                try:
                    i(event)
                except:
                    self.api.say(self.handle_exception())

    def load_module(self, name, path=None):
        print("Loading module {}...".format(name), end=" ", flush=True)
        if path is None:
            for i in self.mod_dirs:
                path = os.path.join(i, name) + ".py"
                if os.path.exists(path):
                    break
            else:
                raise ModuleError("Could not find {}".format(name))
        if name in self.modules:
            # module already loaded; reload
            self.unload_module(name)
        module = importlib.machinery.SourceFileLoader(name, path).load_module()
        # more intelligent way of finding the class inside a module needed here?
        if hasattr(module, "Module"):
            try:
                obj = module.Module(self.api)
            except Exception as e:
                # unregister anything set before loading failed
                self.api.unregister_command(name, True)
                self.api.unregister_listener(name)
                self.api.unregister_filter(name, True)
                raise ModuleError("Failed to instantiate {}".format(name)) from e
            self.modules[name] = obj
        else:
            raise ModuleError("{} does not include a Module class.".format(name))
        print("Loaded")

    def load_modules(self):
        """Load all modules in the modules directory."""
        for j in self.mod_dirs:
            if not os.path.exists(j):
                os.makedirs(j)
            for i in os.listdir(j):
                name, ext = os.path.splitext(i)
                if ext == ".py":
                    if self.whitelist is not None and name not in self.whitelist:
                        continue
                    if self.blacklist is not None and name in self.blacklist:
                        continue
                    try:
                        self.load_module(name, os.path.join(j, i))
                    except:
                        self.handle_exception()

    def unload_module(self, name):
        print("Unloading module {}...".format(name), end=" ", flush=True)
        obj = self.modules[name]
        if hasattr(obj, "unload"):
            try:
                obj.unload()
            except Exception:
                self.handle_exception()
        self.api.unregister_command(name, True)
        self.api.unregister_listener(name)
        self.api.unregister_filter(name, True)
        del self.modules[name]
        print("Unloaded")

    def unload_modules(self):
        """Unload all loaded modules.

        Ignores exceptions on unloading modules. Designed to run cleanup
        procedures for as many modules as possible on exit or core crash."""
        keys = list(self.modules.keys())
        for i in keys:
            try:
                self.unload_module(i)
            except:
                self.handle_exception()

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)

    def get_version(self):
        return "Ximenez bot v{} on Python irclib".format(VERSION)

    def handle_exception(self):
        traceback.print_exc()
        t, v, tb = sys.exc_info()
        if self.verbose_debug >= 2:
            return traceback.format_exception_only(t, v)[-1] + "\n".join(traceback.format_tb(tb)[1 - self.verbose_debug:])
        if self.verbose_debug == 1:
            return traceback.format_exception_only(t, v)[-1]
        return "Internal error."


class API(object):
    """Functions exposed to modules"""
    def __init__(self, bot, maxlines=6):
        self._bot = bot
        self._maxlines = maxlines
        self._last_message_time = 0
        # used for tracking which modules have registered which hooks for use
        # when unloading
        # command words registered by a module
        self._module_commands = collections.defaultdict(set)
        self._no_kwargs = set()
        # (event, fn) pairs registered by a module
        self._module_listeners = collections.defaultdict(set)
        # filters registered by a module
        self._module_filters = collections.defaultdict(set)
        # command/event hooks
        self._command_registry = {}
        self._listener_registry = collections.defaultdict(set)
        self._filter_registry = set()

    def _guarded_call(self, fn, *args, **kwargs):
        try:
            fn(*args, **kwargs)
        except:
            self.say(self.bot.handle_exception())

    def _command_common(self, command, raw=False):
        command = command.lower()
        if not raw:
            command = self._bot.command_char + command
        if command in self._command_registry:
            raise CommandError("Command already registered: {}".format(command))
        return command

    def register_command(self, command, fn, raw=False, no_kwargs=False):
        """Register fn to be called when a user says command.

        If raw is true, the bot's default command character is not prepended to
        the command.

        Subsequent words in the user's line will be passed in as arguments,
        separated by spaces. Shell-style lexing is enabled, i.e. arguments can be
        quoted, spaces backslash-escaped, etc. Keyword arguments are also
        accepted (as argname=argval). Setting no_kwargs=True disables shell
        lexing and keyword argument processing.

        The nickname (nick), whether it is a private message (privmsg), and
        whether the user is an admin (admin) will be passed in as keyword
        arguments.

        Callback functions should be defined with *args or * in the argument list
        to prevent errors with long argument lists overwriting the system keyword
        args."""
        # verify arguments
        sig = inspect.signature(fn).parameters
        for i in "nick", "privmsg", "admin":
            if i not in sig:
                raise CommandError("nick, privmsg and admin kw-only arguments must be defined.")
            if sig[i].kind is not sig[i].KEYWORD_ONLY:
                raise CommandError("nick, privmsg and admin must be kw-only arguments.")
        command = self._command_common(command, raw)
        self._command_registry[command] = fn
        self._module_commands[fn.__func__.__module__].add(command)
        if no_kwargs:
            self._no_kwargs.add(command)

    def register_commands(self, pairs, raw=False):
        """Register multiple commands at once.

        If any command would conflict, no commands are registered. Takes a list
        of (command, fn) pairs."""
        for command, fn in pairs:
            self._command_common(command, raw)
        for command, fn in pairs:
            self.register_command(command, fn, raw)

    def alias_command(self, command, target, raw=False):
        """Mark one command as an alias of another.

        Takes the same arguments as register_command, but the target should be
        the name of another command.

        Currently, this does not allow standard commands to be aliased to raw
        commands or vice versa without manually looking up the command_char."""
        command = self._command_common(command, raw)
        target = target.lower()
        if not raw:
            target = self._bot.command_char + target
        if target not in self._command_registry:
            raise CommandError("Target not found: {}".format(target))
        self._command_registry[command] = target
        # this currently takes the module from the target - I don't think aliasing
        # to commands in other modules is likely to be common
        self._module_commands[self._command_registry[target].__func__.__module__].add(command)

    def register_listener(self, event, fn):
        """Register fn to be called when an event occurs.

        fn will be called with the event object."""
        self._listener_registry[event].add(fn)
        self._module_listeners[fn.__func__.__module__].add((event, fn))

    def register_timer(self, ts, fn, *args, **kwargs):
        """Register fn to be called at a specific time.

        ts can be a timedelta, in which case it will be called in that much
        time, or a datetime, in which case it will be called then.

        args and kwargs, if supplied, will be passed to fn when called."""
        # we're using the private interface for command scheduling here so we
        # can use a UTC scheduled command
        fn = functools.partial(self._guarded_call, fn, *args, **kwargs)
        if isinstance(ts, datetime.timedelta):
            command = XimenezCommand.after(ts, fn)
        elif isinstance(ts, datetime.datetime):
            command = XimenezCommand.at_time(ts, fn)
        else:
            raise ValueError("ts must be a datetime or timedelta")
        self._bot.reactor._schedule_command(command)

    def unregister_command(self, command_or_module, module=False, raw=False):
        """Unregister a particular command, or all commands for a module."""
        if not module:
            command = command_or_module
            if not raw:
                command = self._bot.command_char + command
            del self._command_registry[command]
            if command in self._no_kwargs:
                self._no_kwargs.remove(command)
            for i in self._module_commands.values():
                if command in i:
                    i.remove(command)
                    break
        else:
            module = command_or_module
            for command in self._module_commands[module]:
                del self._command_registry[command]
                if command in self._no_kwargs:
                    self._no_kwargs.remove(command)
            del self._module_commands[module]

    def unregister_listener(self, event_or_module, fn=None):
        """Unregister a listener.

        This can unregister a particular (event, function) pair, or as a convenience
        method, unregister all listeners for a module."""
        if fn:
            event = event_or_module
            for i in self._module_listeners.values():
                if (event, fn) in i:
                    i.remove((event, fn))
                    break
            self._listener_registry[event].remove(fn)
        else:
            module = event_or_module
            for event, fn in self._module_listeners[module]:
                self._listener_registry[event].remove(fn)
            del self._module_listeners[module]

    def get_timers(self, fn):
        """Return all timers calling the function fn."""
        if fn is None:
            return [i for i in self._bot.reactor.delayed_commands if i.function.args]
        return [i for i in self._bot.reactor.delayed_commands if i.function.args and i.function.args[0] == fn]

    def unregister_timer(self, fn, n):
        """Delete the nth timer calling function fn."""
        # this is a little bit of a messy way of removing timers compared to
        # the command and event registration, but timers have an annoying habit
        # of getting executed and disappearing, making expecting modules to
        # track all the timers they've executed a bit impractical
        d = self._bot.reactor.delayed_commands
        count = 0
        for i in range(len(d)):
            if d[i].function.args[0] == fn:
                if count == n:
                    del d[i]
                    return
                count += 1
        raise IndexError("Command {} not found.".format(n))

    def register_filter(self, fn):
        """Register a function as an event filter.

        fn will be called with the event object. It should return True to block
        the event or False to allow it."""
        self._filter_registry.add(fn)
        self._module_filters[fn.__func__.__module__].add(fn)

    def unregister_filter(self, fn_or_module, module=False):
        """Unregister a function, or all functions belonging to a named module."""
        if module:
            for fn in self._module_filters[fn_or_module]:
                if fn in self._filter_registry:
                    self._filter_registry.remove(fn)
            del self._module_filters[fn_or_module]
        else:
            self._filter_registry.remove(fn_or_module)

    def say(self, *message, target=None, length_override=False):
        """Say a message.

        Will be posted to the channel if target is not supplied, or privately
        to target.

        Split up and rate limited. The number of lines which can be posted at
        once is also limited to avoid long event loop lockups, although this
        can be overriden if necessary - use with caution."""
        msg = " ".join(str(i) for i in message)
        # sanitise \r characters in case of line ending issues
        msg = msg.replace("\r", "")
        lines = msg.split("\n")
        if target is None:
            target = self._bot.channel
        count = 0
        t = time.time()
        if t-1 < self._last_message_time:
            time.sleep(self._last_message_time + 1 - t)
        for i in lines:
            while i:
                # break long lines to avoid truncation
                msg, i = i[:425], i[425:]
                count += 1
                if not length_override and count > self._maxlines:
                    break
                self._bot.connection.privmsg(target, msg)
                time.sleep(1)
            self._last_message_time = time.time()
            if not length_override and count > self._maxlines:
                self._bot.connection.privmsg(target, "<long message truncated>")
                break

    def address_say(self, target, *msg, privmsg=False, length_override=False):
        """Say a message to a person.

        Like say() but prepends the target's name to public messages."""
        if not privmsg:
            self.say(target+":", *msg, length_override=length_override)
        else:
            self.say(*msg, target=target, length_override=length_override)

    _nickname_regex = re.compile(r"[A-Za-z_\[\]\\^{}|`][A-Za-z0-9_\-\[\]\\^{}|`]*")
    def nickname_trim(self, nick):
        """Extract a valid nickname from a string.

        This is designed to strip, say, trailing colons or commas from a name
        (which might be added by tab completion) or extract a name from angle
        brackets."""
        m = self._nickname_regex.search(nick)
        if not m:
            return None
        return m.group(0)

    @property
    def userlist(self):
        """Get the current channel userlist."""
        return self._bot.channels[self._bot.channel].users()

    @property
    def name(self):
        """Get the bot's current nickname."""
        return self._bot.connection.get_nickname()

    @property
    def channel(self):
        """Get the current channel."""
        # TODO this should return the channel for the current event once
        # multiple channels are handled
        return self._bot.channel

    @property
    def bot(self):
        """Return the actual bot object.

        Required for some modules, but not generally recommended."""
        return self._bot

    @property
    def maxlines(self):
        """Return the maximum lines in a single message."""
        return self._maxlines

    @property
    def instance_name(self):
        """Return the name of the instance in a multi-instance setup."""
        return self._bot.instance_name


class CommandError(Exception):
    pass


class ModuleError(Exception):
    pass
