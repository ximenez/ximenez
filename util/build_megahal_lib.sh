#! /bin/sh
# builds a MegaHAL shared library from the MegaHAL source dir
gcc -shared -fPIC -o libmegahal.so megahal.c
